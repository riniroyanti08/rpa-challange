<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit</name>
   <tag></tag>
   <elementGuidId>bc172bff-f6b6-4d20-996e-2d0ab4269311</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.btn.uiColorButton</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=button[name=&quot;Submit&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0de27095-7110-4459-ac55-2843946197e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn uiColorButton</value>
      <webElementGuid>a6f557cd-10fa-4751-9db7-0ee363e37eb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>1f4040e8-91c6-4d7a-898d-bfbc12edc43e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Submit</value>
      <webElementGuid>22d3d939-1ba6-4cc8-be6c-ba547b75c127</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/div[@class=&quot;body row1 scroll-y&quot;]/app-rpa1[1]/div[@class=&quot;row parent&quot;]/div[@class=&quot;inputFields col s6 m6 l6&quot;]/form[@class=&quot;ng-untouched ng-pristine ng-invalid&quot;]/input[@class=&quot;btn uiColorButton&quot;]</value>
      <webElementGuid>1ece9c4f-a136-44eb-98c4-a20482bf6f7b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Submit']</value>
      <webElementGuid>6bbce07a-dc79-44e6-bf17-613dbdc4ad5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/input</value>
      <webElementGuid>50a16474-2441-45bb-ba23-77bf157a5ded</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit']</value>
      <webElementGuid>c1ed98f9-a07b-4766-b536-1f787981afcc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
